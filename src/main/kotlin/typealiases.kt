package react.materialui

import org.w3c.dom.events.Event

typealias EventHandlerFunction = (Event) -> Unit